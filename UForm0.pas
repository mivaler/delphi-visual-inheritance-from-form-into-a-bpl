unit UForm0;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ImgList, System.Actions,
  Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.Menus,
  Vcl.ComCtrls;

type
  TForm0 = class(TForm)
    MainMenu1: TMainMenu;
    StatusBar1: TStatusBar;
    Archivo1: TMenuItem;
    ImageList1: TImageList;
    New1: TMenuItem;
    Open1: TMenuItem;
    Close1: TMenuItem;
    About1: TMenuItem;
    About2: TMenuItem;
    ActionList1: TActionList;
    actnFileNew: TAction;
    actnFileOpen: TAction;
    actnFileSave: TAction;
    actnHelpAbout: TAction;
    procedure actnFileNewExecute(Sender: TObject);
    procedure actnFileOpenExecute(Sender: TObject);
    procedure actnFileSaveExecute(Sender: TObject);
    procedure actnHelpAboutExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form0: TForm0;

implementation

{$R *.dfm}

procedure TForm0.actnFileSaveExecute(Sender: TObject);
begin
  ShowMessage('File>Save');
end;

procedure TForm0.actnHelpAboutExecute(Sender: TObject);
begin
  ShowMessage('Help>About');
end;

procedure TForm0.actnFileNewExecute(Sender: TObject);
begin
  ShowMessage('File>New');
end;

procedure TForm0.actnFileOpenExecute(Sender: TObject);
begin
  ShowMessage('File>Open');
end;

end.
