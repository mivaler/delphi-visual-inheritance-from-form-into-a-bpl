unit UMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UForm0, System.Actions, Vcl.ActnList,
  Vcl.ImgList, Vcl.Menus, Vcl.ComCtrls, Vcl.StdCtrls;

type
  TForm1 = class(TForm0)
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  inherited;
  Edit2.Text := Edit1.Text;
end;

end.
